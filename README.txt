----------------------------
ENTERPRISE APP
----------------------------





----------------------------
Device Requirements
----------------------------

Runs on 2.3.* or greater ( API Level 11 or greater)



----------------------------
Emulator Requirements
----------------------------

1. Runs on Genymotion Virtual Devices 4.3.* or greater.


2. Requires Google Services to be installed and updated in Genymotion Virtual Device .


3. Change the google maps API Key in the application manifest .
   In order to obtain the maps API key , follow this link .

	https://developers.google.com/maps/documentation/android/start#getting_the_google_maps_android_api_v2


