package com.krahul.genericapp.URLHELPER;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import android.util.Log;

public class UrlHelper {

	private URL url = null;
	private URLConnection urlconn = null;
	private BufferedReader in = null;
	private String jsonstring = null;
	private final static String TAG = "DEBUG";

	// For Geocoding
	public String getJSONString(String u) {
		try {
			url = new URL(u);
			urlconn = url.openConnection();
			// urlconn.setRequestProperty ("Authorization", "Basic " +
			// encodedUnamePwd);
			in = new BufferedReader(new InputStreamReader(
					urlconn.getInputStream()));

			// For JSON thingy.
			StringBuilder sb = new StringBuilder();
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				sb.append(inputLine + "\n");
			}// end while

			jsonstring = sb.toString();
			in.close();

		}// end try
		catch (IOException e) {
			// TODO Auto-generated catch block
			Log.d(TAG, "Address not in the proper format");
			e.printStackTrace();
		}
		return jsonstring;
	}

}
