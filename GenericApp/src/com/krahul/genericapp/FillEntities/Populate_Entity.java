package com.krahul.genericapp.FillEntities;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.krahul.genericapp.Configuration;
import com.krahul.genericapp.BeanEntities.ContactEntity;
import com.krahul.genericapp.BeanEntities.IEntity;
import com.krahul.genericapp.BeanEntities.MapEntity;
import com.krahul.genericapp.BeanEntities.PartnerEntity;

public class Populate_Entity {

	private static final String TAG = "In Popluate Entity Class";

	public static IEntity populateEntity(IEntity entityObj, Cursor result,
			Context context, Configuration configObject)
			throws NoSuchMethodException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, JSONException {

		return entityObj.setData(result, entityObj, context, configObject);

	}

	public static List<MapEntity> populateMapEntity(
			List<MapEntity> mapEntityList, Cursor result) {

		result.moveToFirst();

		do {

			Double lat = result.getDouble(result.getColumnIndex("Latitude"));
			Double longi = result.getDouble(result.getColumnIndex("Latitude"));

			//SomeHow the cursor is reading the Lat=null in SQLite as 0.0 . Confused :( 
			// Using 0.0 as of now . but have to change it :( .
			if (lat != 0.0 && longi != 0.0) {
				MapEntity mapEntityObject = new MapEntity();

				mapEntityObject.setID(result.getString(result
						.getColumnIndex("ENTITY_ID")));
				mapEntityObject.setLatitude(result.getDouble(result
						.getColumnIndex("Latitude")));
				mapEntityObject.setLongitude(result.getDouble(result
						.getColumnIndex("Longitude")));
				mapEntityObject.setSchemaType(result.getString(result
						.getColumnIndex("SchemaType")));

				if (mapEntityObject.getSchemaType().equals("Partner")) {

					mapEntityObject.setColor(0.0f);
				} else if (mapEntityObject.getSchemaType().equals("Contact")) {
					mapEntityObject.setColor(60.0f);
				} else {
					mapEntityObject.setColor(100.0f);
				}
				mapEntityList.add(mapEntityObject);
			} else
				Log.d(TAG, "Lats and Longs are null");
		} while (result.moveToNext());

		return mapEntityList;

	}

}
