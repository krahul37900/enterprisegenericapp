package com.krahul.genericapp;

import java.util.HashMap;
import java.util.Map;

public class RelatedDataConfiguration {

	private String RelationID;
	private String KEY;
		
	public String getKEY() {
		return KEY;
	}

	public void setKEY(String kEY) {
		KEY = kEY;
	}

	public String getRelationID() {
		return RelationID;
	}

	public void setRelationID(String relationID) {
		RelationID = relationID;
	}

	public Map<String, String> getField2Label() {
		return Field2Label;
	}

	public void setField2Label(Map<String, String> field2Label) {
		Field2Label = field2Label;
	}

	public Map<String, String> getField2Value() {
		return Field2Value;
	}

	public void setField2Value(Map<String, String> field2Value) {
		Field2Value = field2Value;
	}

	private Map<String, String> headerLabel = new HashMap<String, String>();
	private Map<String, String> headerValue = new HashMap<String, String>();
	private Map<String, String> Field1Label = new HashMap<String, String>();
	private Map<String, String> Field1Value = new HashMap<String, String>();
	private Map<String, String> Field2Label = new HashMap<String, String>();
	private Map<String, String> Field2Value = new HashMap<String, String>();

	public Map<String, String> getHeaderLabel() {
		return headerLabel;
	}

	public void setHeaderLabel(Map<String, String> headerLabel) {
		this.headerLabel = headerLabel;
	}

	public Map<String, String> getHeaderValue() {
		return headerValue;
	}

	public void setHeaderValue(Map<String, String> headerValue) {
		this.headerValue = headerValue;
	}

	public Map<String, String> getField1Label() {
		return Field1Label;
	}

	public void setField1Label(Map<String, String> field1Label) {
		Field1Label = field1Label;
	}

	public Map<String, String> getField1Value() {
		return Field1Value;
	}

	public void setField1Value(Map<String, String> field1Value) {
		Field1Value = field1Value;
	}

}
