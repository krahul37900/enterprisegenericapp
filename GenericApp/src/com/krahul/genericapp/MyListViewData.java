package com.krahul.genericapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyListViewData extends ArrayAdapter<RelatedDataConfiguration>{

	private List<RelatedDataConfiguration> data = new ArrayList<RelatedDataConfiguration>();
	private static final String TAG="IN CUSTOM ADAPTER CLASS";
	
	public MyListViewData(Context context, int resource,
		 List<RelatedDataConfiguration> objects) {
		super(context, resource, objects);
		this.data=objects;
		Log.d(TAG,"INSIDE THE CONSTruCTOR");
		
		
	}

	@Override
	public RelatedDataConfiguration getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public int getCount() {
		Log.d(TAG,String.valueOf(data.size()));
		return data.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		
		if (v == null) {

	        LayoutInflater vi;
	        vi = LayoutInflater.from(getContext());
	        v = vi.inflate(R.layout.listviewitem, null);

	    }
		
		RelatedDataConfiguration item = data.get(position);
		

	    if (item != null) {

	        TextView textView1 = (TextView) v.findViewById(R.id.textView1);
	        TextView textView2 = (TextView) v.findViewById(R.id.textView2);
	        
	        if (textView1 != null) {
	            textView1.setText(item.getField1Value().get("FIELD1"));
	        }
	        if (textView2 != null) {

	            textView2.setText( item.getField2Value().get("FIELD2"));
	        }
	        
	    }

		return v;
	}
	
	
	
}