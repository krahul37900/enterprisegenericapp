package com.krahul.genericapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.krahul.genericapp.BeanEntities.ContactEntity;
import com.krahul.genericapp.BeanEntities.MapEntity;
import com.krahul.genericapp.BeanEntities.PartnerEntity;
import com.krahul.genericapp.BeanEntities.SalesOrdersEntity;
import com.krahul.genericapp.Database.DataBaseHelper;
import com.krahul.genericapp.FillEntities.Populate_Entity;
import com.krahul.genericapp.URLHELPER.UrlHelper;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements AsyncResponse,
		OnMarkerClickListener {

	private final String TAG = "MAIN ACTIVITY";

	private DataBaseHelper dbhelper = null;
	private GoogleMap mMap;
	private List<MapEntity> mapEntityList = new ArrayList<MapEntity>();

	private void setUpMapIfNeeded() {

		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			mMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				// The Map is verified. It is now safe to manipulate the map.

			}
		}
	}


	private boolean checkNetworkStatus() {

		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			Log.d(TAG, "YES INTERNET IS AVAILABLE");
			return true;
		} else {
			Toast.makeText(getBaseContext(), "No Internet Service",
					Toast.LENGTH_LONG).show();
			Log.d(TAG, "No Network");
			return false;
		}
	}

	private class URlParse extends AsyncTask<String, String, String> {

		private AsyncResponse callback;

		public URlParse(Activity act) {

			this.callback = (AsyncResponse) act;
		}

		@Override
		protected String doInBackground(String... params) {

			UrlHelper u = new UrlHelper();

			// Purely from Mongo DB
			String url1 = "https://mongolab.com/api/1/databases/mas/collections/entityCollectionGeneric?apiKey=qufEkksXFn-sFJmgPIZXB5S-4vKL5dNc";

			String json1 = u.getJSONString(url1);
			Log.d(TAG, json1);
			return json1;
		}

		@Override
		protected void onPostExecute(String jsonString) {

			Log.d(TAG, "ON POST EXECUTE");
			callback.processFinish(jsonString);

		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
				
		
		setUpMapIfNeeded(); // setup map
		mMap.setOnMarkerClickListener((OnMarkerClickListener) this); // Set
																		// Marker
																		// Listener.
		checkNetworkStatus(); // Check Network connectivity status
		CreateOrLookUpLocalDB(); // Set up or load up from the cache.

	}

	

	private void CreateOrLookUpLocalDB() {

		try {
			dbhelper = new DataBaseHelper(this);
			if (dbhelper.checkDataBase() == true) {

				dbhelper.openDataBase(); // Open database for querying.
				// Read MapConfig here ..
				String query = "SELECT * FROM EntityTable where SchemaType IN ("
						+ ConfigReader.readConfig("MapConfig1", this) + ");"; // Read
																				// from
																				// config
																				// file.

				mapEntityList = Populate_Entity.populateMapEntity(
						mapEntityList, dbhelper.Query(query));
				load_markers(mapEntityList); // Display the markers.

			} // end if
			else
				new URlParse(this).execute();
		} // end of try
		catch (Exception e) {
			e.printStackTrace();
		} finally {

			dbhelper.close();
		}

	}

	private void load_markers(List<MapEntity> mapEntityList) {

		for (int i = 0; i < mapEntityList.size(); i++) {

			MapEntity temp = mapEntityList.get(i);
			temp.setMapMarker(mMap.addMarker(new MarkerOptions().position(
					new LatLng(temp.getLatitude(), temp.getLongitude())).icon(
					BitmapDescriptorFactory.defaultMarker(temp.getColor()))));

		}

	}

	@Override
	public void processFinish(String output) {

		Log.d(TAG, "Data Refresh Successful");
		Toast.makeText(getBaseContext(), "Refresh Successfull ",
				Toast.LENGTH_LONG).show();

		dbhelper = new DataBaseHelper(this); // Caching the data into SQLite
												// Databases

		try {

			dbhelper.createDataBase(); // Create if database does not exist
			dbhelper.openDataBase(output); // OpenDatabase and Create Table if
											// not exists.
			String query = "SELECT * FROM EntityTable where SchemaType IN ("
					+ ConfigReader.readConfig("MapConfig1", this) + ");"; // Read
																			// from
																			// config
																			// file.

			mapEntityList = Populate_Entity.populateMapEntity(mapEntityList,
					dbhelper.Query(query));
			load_markers(mapEntityList); // Display the markers.

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			dbhelper.close(); // Close the database.
		}

	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		Log.d(TAG, "Inside OnMarkerClick");

		for (int i = 0; i < mapEntityList.size(); i++) {

			MapEntity mapEntityTemp = mapEntityList.get(i);

			if (mapEntityTemp.getMapMarker().getPosition()
					.equals(marker.getPosition())) {

				Log.d(TAG, "marker clicked ");

				Intent intent = new Intent(this, DetailedView.class); // Send
																		// the
																		// relData
																		// jsonString
																		// info
																		// with
																		// the
																		// intent.
				Log.d(TAG, mapEntityTemp.getSchemaType());
				intent.putExtra("SCHEMA TYPE", mapEntityTemp.getSchemaType());
				intent.putExtra("ID", mapEntityTemp.getID()); // Send the
																// PartnerID and
																// context(which
																// is the schema
																// Type).
				startActivity(intent);
				break;
			}
		}

		return true;
	}

	public void OnRefresh(View v) {

		Toast.makeText(getBaseContext(), "Refreshing .... ", Toast.LENGTH_LONG)
				.show();

		if (checkNetworkStatus() == true) {
			mMap.clear();
			new URlParse(this).execute();
		} else {
			Toast.makeText(getBaseContext(), "Refresh Not Possible",
					Toast.LENGTH_LONG).show();
		}

	}

}
