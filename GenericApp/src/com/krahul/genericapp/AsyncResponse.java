package com.krahul.genericapp;

// Interface in order to get control back to the main thread in the Main Activity UI .
public interface AsyncResponse {

	void processFinish(String output);
}
