package com.krahul.genericapp.Database;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DataBaseHelper extends SQLiteOpenHelper {

	// The Android's default system path of your application database.
	@SuppressLint("SdCardPath")
	private static String DB_PATH = "/data/data/com.krahul.genericapp/databases/";
	private static String DB_NAME = "Entity.db";
	private SQLiteDatabase myDataBase;
	private final Context myContext;
	private final static String TAG = "IN SQLHELPER CLASS";

	private static class UserTable {
		private static final String NAME = "EntityTable";
		private static String ID = "ENTITY_ID";
		private static String SchemaType = "SchemaType";
		private static String JsonData = "Data";
		private static String Latitude = "Latitude";
		private static String Longitude = "Longitude";
		private static String relData = "RelData";
		private static String updatedAt = "UpdatedAt";
	}

	public DataBaseHelper(Context context) {
		super(context, DB_NAME, null, 1);
		this.myContext = context;

	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {

		boolean dbExist = checkDataBase();

		if (dbExist) {

			Log.d(TAG, "DB EXISTS");
			// do nothing - database already exist
		} else {

			// By calling this method and empty database will be created into
			// the default system path
			// of your application so we are gonna be able to overwrite that
			// database with our database.
			this.getReadableDatabase();

			/*
			 * try {
			 * 
			 * copyDataBase();
			 * 
			 * } catch (IOException e) {
			 * 
			 * throw new Error("Error copying database");
			 * 
			 * }
			 */

		}// end else

	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	@SuppressWarnings("finally")
	public boolean checkDataBase() {
		SQLiteDatabase checkDB = null;
		try {

			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READONLY);
		} catch (SQLiteException e) {
			Log.d(TAG, "DATABASE NOT FOUND ");
			// e.printStackTrace();
		} finally {
			if (checkDB != null)
				checkDB.close();
			return checkDB != null ? true : false;

		}
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	/*
	 * private void copyDataBase() throws IOException{
	 * 
	 * //Open your local db as the input stream InputStream myInput =
	 * myContext.getAssets().open(DB_NAME);
	 * 
	 * // Path to the just created empty db String outFileName = DB_PATH +
	 * DB_NAME;
	 * 
	 * //Open the empty db as the output stream OutputStream myOutput = new
	 * FileOutputStream(outFileName);
	 * 
	 * //transfer bytes from the inputfile to the outputfile byte[] buffer = new
	 * byte[1024]; int length; while ((length = myInput.read(buffer))>0){
	 * myOutput.write(buffer, 0, length); }
	 * 
	 * //Close the streams myOutput.flush(); myOutput.close(); myInput.close();
	 * 
	 * }
	 */
	// Method called while reading the created database for querying purposes.
	public void openDataBase() throws SQLException {
		// Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READONLY);

	}

	public void openDataBase(String jsonString) throws SQLException,
			JSONException {

		// Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READWRITE);
		dropTable();
		createTable();
		insertData(jsonString);

	}

	private void dropTable() {
		myDataBase.execSQL("DROP TABLE IF EXISTS " + UserTable.NAME);

	}

	private void createTable() throws SQLException {

		Log.d("SQL", "CREATING TABLE");
		myDataBase
				.execSQL(String
						.format("CREATE TABLE %s (%s TEXT, %s TEXT, %s TEXT, %s DOUBLE , %s DOUBLE , %s TEXT, %s TEXT)",
								UserTable.NAME, UserTable.ID,
								UserTable.SchemaType, UserTable.JsonData,
								UserTable.Latitude, UserTable.Longitude,
								UserTable.relData, UserTable.updatedAt));
		Log.d("SQL", "Creation Done");

	}

	private void insertData(String jsonString) throws JSONException {

		JSONArray jarray = new JSONArray(jsonString);

		Log.d("SQL", "INSERTING DATA INTO TABLE");
		Log.d("SQL", String.valueOf(jarray.length()));

		for (int i = 0; i < jarray.length(); i++) {

			JSONObject jobj = jarray.getJSONObject(i);

			String ID = jobj.getString("_id");
			String schema_type = jobj.getString("schemaType");
			String temp = jobj.getString("data");
			String jsonReplaced = temp.replace("'", "*");

			Double lat = null;
			Double longi = null;

			if ((jobj.isNull("Latitude") && jobj.isNull("Longitude")) == false) {
				lat = jobj.getDouble("Latitude");
				longi = jobj.getDouble("Longitude");
			}

			String relData = null;

			if (jobj.isNull("relData") == false) {
				relData = jobj.getString("relData");
			}

			String updatedAt = jobj.getString("updatedAt");

			Log.d(TAG, String.valueOf(lat));

			this.myDataBase
					.execSQL("INSERT INTO EntityTable(ENTITY_ID,SchemaType,Data,Latitude,Longitude,relData,UpdatedAt) VALUES ('"
							+ ID
							+ "','"
							+ schema_type
							+ "','"
							+ jsonReplaced
							+ "','"
							+ lat
							+ "','"
							+ longi
							+ "','"
							+ relData
							+ "','" + updatedAt + "');");
			// this.myDataBase.execSQL("INSERT INTO EntityTable(Partner_ID,SchemaType,Latitude,Longitude,UpdatedAt) VALUES ('"+ID+"','"+schema_type+"','"+lat+"','"+longi+"','"+updatedAt+"');");

		} // end of for loop.

		Log.d("SQL", "INSERTION COMPLETE");
	}

	@Override
	public synchronized void close() {

		if (myDataBase != null)
			myDataBase.close();

		super.close();

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public Cursor Query(String query) {

		return myDataBase.rawQuery(query, null);

	}

	/*
	 * public ArrayList<DBandBeanEntity> QueryTable() {
	 * 
	 * ArrayList<DBandBeanEntity> dbbean = new ArrayList<DBandBeanEntity>();
	 * 
	 * Cursor c = myDataBase.rawQuery("SELECT * from "+UserTable.NAME, null);
	 * c.moveToFirst();
	 * 
	 * Log.d(TAG,String.valueOf(c.getCount()));
	 * 
	 * 
	 * do { DBandBeanEntity temp = new DBandBeanEntity();
	 * 
	 * temp.jresults=c.getString(c.getColumnIndex("Data")).replace("*","'");
	 * temp.latitude=c.getDouble(c.getColumnIndex("Latitude"));
	 * temp.longitude=c.getDouble(c.getColumnIndex("Longitude"));
	 * 
	 * Log.d("INSIDE SQL QUERY TABLE",String.valueOf(temp.latitude) +
	 * String.valueOf(temp.longitude) );
	 * 
	 * dbbean.add(temp);
	 * 
	 * }while(c.moveToNext());
	 * 
	 * Log.d(TAG,String.valueOf(dbbean.size()));
	 * 
	 * c.close();
	 * 
	 * return dbbean; }
	 */

}
