package com.krahul.genericapp;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

public class ConfigReader {

	private static final String TAG = "In Config Reader";

	public static String readConfig(String configFile, Context context) {

		AssetManager assetManager = context.getAssets();
		StringBuilder s = null;
		InputStream in = null;
		try {

			in = assetManager.open(configFile);
			InputStreamReader isr = new InputStreamReader(in);
			BufferedReader r = new BufferedReader(isr);

			s = new StringBuilder();
			String line = null;

			while ((line = r.readLine()) != null) {
				s.append(line);
			}
			Log.d(TAG, s.toString());

		} catch (Exception e) {
			Log.d(TAG, "Exception in Config File Reader");
			e.printStackTrace();
		} finally {
			// Do nothing.
			// Close stream readers.
		}

		return s.toString();
	}

}
