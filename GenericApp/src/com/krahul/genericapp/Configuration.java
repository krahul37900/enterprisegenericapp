package com.krahul.genericapp;

import java.util.HashMap;
import java.util.Map;

public class Configuration {

	// UI Fields
	private Map<String, String> HeaderLabel = new HashMap<String, String>();
	private Map<String, String> HeaderValue = new HashMap<String, String>();
	private Map<String, String> Field1Label = new HashMap<String, String>();
	private Map<String, String> Field1Value = new HashMap<String, String>();
	private Map<String, String> Field2Label = new HashMap<String, String>();
	private Map<String, String> Field2Value = new HashMap<String, String>();
	private Map<String, String> Field3Label = new HashMap<String, String>();
	private Map<String, String> Field3Value = new HashMap<String, String>();
	private Map<String, String> Field4Label = new HashMap<String, String>();
	private Map<String, String> Field4Value = new HashMap<String, String>();

	// Rel Data fields

	private RelatedDataConfiguration rel1 = null;
	private RelatedDataConfiguration rel2 = null;

	public RelatedDataConfiguration getRel1() {
		return rel1;
	}

	public void setRel1(RelatedDataConfiguration rel1) {
		this.rel1 = rel1;
	}

	public RelatedDataConfiguration getRel2() {
		return rel2;
	}

	public void setRel2(RelatedDataConfiguration rel2) {
		this.rel2 = rel2;
	}

	public Map<String, String> getHeaderLabel() {
		return HeaderLabel;
	}

	public void setHeaderLabel(Map<String, String> headerLabel) {
		HeaderLabel = headerLabel;
	}

	public Map<String, String> getHeaderValue() {
		return HeaderValue;
	}

	public void setHeaderValue(Map<String, String> headerValue) {
		HeaderValue = headerValue;
	}

	public Map<String, String> getField1Label() {
		return Field1Label;
	}

	public void setField1Label(Map<String, String> field1Label) {
		Field1Label = field1Label;
	}

	public Map<String, String> getField1Value() {
		return Field1Value;
	}

	public void setField1Value(Map<String, String> field1Value) {
		Field1Value = field1Value;
	}

	public Map<String, String> getField2Label() {
		return Field2Label;
	}

	public void setField2Label(Map<String, String> field2Label) {
		Field2Label = field2Label;
	}

	public Map<String, String> getField2Value() {
		return Field2Value;
	}

	public void setField2Value(Map<String, String> field2Value) {
		Field2Value = field2Value;
	}

	public Map<String, String> getField3Label() {
		return Field3Label;
	}

	public void setField3Label(Map<String, String> field3Label) {
		Field3Label = field3Label;
	}

	public Map<String, String> getField3Value() {
		return Field3Value;
	}

	public void setField3Value(Map<String, String> field3Value) {
		Field3Value = field3Value;
	}

	public Map<String, String> getField4Label() {
		return Field4Label;
	}

	public void setField4Label(Map<String, String> field4Label) {
		Field4Label = field4Label;
	}

	public Map<String, String> getField4Value() {
		return Field4Value;
	}

	public void setField4Value(Map<String, String> field4Value) {
		Field4Value = field4Value;
	}

}
