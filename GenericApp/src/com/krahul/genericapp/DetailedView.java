package com.krahul.genericapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.krahul.genericapp.BeanEntities.ContactEntity;
import com.krahul.genericapp.BeanEntities.EntityFactory;
import com.krahul.genericapp.BeanEntities.IEntity;
import com.krahul.genericapp.BeanEntities.PartnerEntity;

import com.krahul.genericapp.BeanEntities.SalesOrdersEntity;

import com.krahul.genericapp.Database.DataBaseHelper;
import com.krahul.genericapp.FillEntities.Populate_Entity;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class DetailedView extends Activity {

	private DataBaseHelper dbhelper = null; // Database Object
	private IEntity entityObject = null;
	private Configuration configObject = null;

	private final static String TAG = "In Detailed View"; // Debug String

	private void setMapButtonListener() {
		ImageButton img = (ImageButton) findViewById(R.id.imageButton1); // Set
																			// ImageButton
																			// Click
																			// Listener.
		img.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(DetailedView.this, MainActivity.class);
				startActivity(i);
			}
		});
	}

	private void setUIFields() throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {

		Log.d(TAG, (configObject.getHeaderLabel().get("name")));
		this.setTitle(configObject.getHeaderLabel().get("name")
				+ " : "
				+ entityObject.getData(configObject.getHeaderValue()
						.get("name"))); // Set the Title of detailedView.

		Log.d(TAG, "Starting UI task");

		// TextView 1 properties.
		TextView textView1 = (TextView) findViewById(R.id.textView1);
		textView1.setText(configObject.getField1Label().get("name"));
		textView1.setTextColor(Color.parseColor(configObject.getField1Label()
				.get("color")));

		// EditText1 properties
		EditText editText1 = (EditText) findViewById(R.id.editText1);
		editText1.setKeyListener(null);
		editText1.setText(entityObject.getData(configObject.getField1Value()
				.get("name")));

		editText1.setTextColor(Color.parseColor(configObject.getField1Value()
				.get("color")));

		// textView 2 properties
		TextView textView2 = (TextView) findViewById(R.id.textView2);
		textView2.setText(configObject.getField2Label().get("name"));
		textView2.setTextColor(Color.parseColor(configObject.getField2Label()
				.get("color")));

		// EditText2 properties.
		EditText editText2 = (EditText) findViewById(R.id.editText2);
		editText2.setKeyListener(null);
		editText2.setText(entityObject.getData(configObject.getField2Value()
				.get("name")));
		editText2.setTextColor(Color.parseColor(configObject.getField2Value()
				.get("color")));

		// textView 3 properties
		TextView textView3 = (TextView) findViewById(R.id.textView3);
		textView3.setText(configObject.getField3Label().get("name"));
		textView3.setTextColor(Color.parseColor(configObject.getField3Label()
				.get("color")));

		// EditText3 properties.
		EditText editText3 = (EditText) findViewById(R.id.editText3);
		editText3.setKeyListener(null);
		editText3.setText(entityObject.getData(configObject.getField3Value()
				.get("name")));
		editText3.setTextColor(Color.parseColor(configObject.getField3Value()
				.get("color")));

		// textView 4 properties
		TextView textView4 = (TextView) findViewById(R.id.textView4);
		textView4.setText(configObject.getField4Label().get("name"));
		textView4.setTextColor(Color.parseColor(configObject.getField4Label()
				.get("color")));

		// EditText 4 properties.
		EditText editText4 = (EditText) findViewById(R.id.editText4);
		editText4.setKeyListener(null);
		editText4.setText(entityObject.getData(configObject.getField4Value()
				.get("name")));
		editText4.setTextColor(Color.parseColor(configObject.getField4Value()
				.get("color")));

		Log.d(TAG, "FINISHED UI tasks");

	}

	private void loadConfig(String config) throws JSONException {

		configObject = new Configuration();
		JSONObject jobj = new JSONObject(ConfigReader.readConfig(config, this));

		// header
		JSONObject headerlabelJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Header").getJSONObject("label");
		Map<String, String> headerLabel = new HashMap<String, String>();
		headerLabel.put("name", headerlabelJSON.getString("name"));
		headerLabel.put("visible", headerlabelJSON.getString("visible"));
		headerLabel.put("color", headerlabelJSON.getString("color"));
		configObject.setHeaderLabel(headerLabel);

		JSONObject headerValueJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Header").getJSONObject("value");
		Map<String, String> headerValue = new HashMap<String, String>();
		headerValue.put("name", headerValueJSON.getString("name"));
		headerValue.put("visible", headerValueJSON.getString("visible"));
		headerValue.put("color", headerValueJSON.getString("color"));
		configObject.setHeaderValue(headerValue);

		// Field 1
		JSONObject field1labelJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Field1").getJSONObject("label");
		Map<String, String> field1Label = new HashMap<String, String>();
		field1Label.put("name", field1labelJSON.getString("name"));
		field1Label.put("visible", field1labelJSON.getString("visible"));
		field1Label.put("color", field1labelJSON.getString("color"));
		configObject.setField1Label(field1Label);

		JSONObject field1ValueJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Field1").getJSONObject("value");
		Map<String, String> field1Value = new HashMap<String, String>();
		field1Value.put("name", field1ValueJSON.getString("name"));
		field1Value.put("visible", field1ValueJSON.getString("visible"));
		field1Value.put("color", field1ValueJSON.getString("color"));
		configObject.setField1Value(field1Value);

		// Field 2
		JSONObject field2labelJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Field2").getJSONObject("label");
		Map<String, String> field2Label = new HashMap<String, String>();
		field2Label.put("name", field2labelJSON.getString("name"));
		field2Label.put("visible", field2labelJSON.getString("visible"));
		field2Label.put("color", field2labelJSON.getString("color"));
		configObject.setField2Label(field2Label);

		JSONObject field2ValueJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Field2").getJSONObject("value");
		Map<String, String> field2Value = new HashMap<String, String>();
		field2Value.put("name", field2ValueJSON.getString("name"));
		field2Value.put("visible", field2ValueJSON.getString("visible"));
		field2Value.put("color", field2ValueJSON.getString("color"));
		configObject.setField2Value(field2Value);

		// Field 3
		JSONObject field3labelJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Field3").getJSONObject("label");
		Map<String, String> field3Label = new HashMap<String, String>();
		field3Label.put("name", field3labelJSON.getString("name"));
		field3Label.put("visible", field3labelJSON.getString("visible"));
		field3Label.put("color", field3labelJSON.getString("color"));
		configObject.setField3Label(field3Label);

		JSONObject field3ValueJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Field3").getJSONObject("value");
		Map<String, String> field3Value = new HashMap<String, String>();
		field3Value.put("name", field3ValueJSON.getString("name"));
		field3Value.put("visible", field3ValueJSON.getString("visible"));
		field3Value.put("color", field3ValueJSON.getString("color"));
		configObject.setField3Value(field3Value);

		// Field 4
		JSONObject field4labelJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Field4").getJSONObject("label");
		Map<String, String> field4Label = new HashMap<String, String>();
		field4Label.put("name", field4labelJSON.getString("name"));
		field4Label.put("visible", field4labelJSON.getString("visible"));
		field4Label.put("color", field4labelJSON.getString("color"));
		configObject.setField4Label(field4Label);

		JSONObject field4ValueJSON = jobj.getJSONObject("Detail")
				.getJSONObject("Field4").getJSONObject("value");
		Map<String, String> field4Value = new HashMap<String, String>();
		field4Value.put("name", field4ValueJSON.getString("name"));
		field4Value.put("visible", field4ValueJSON.getString("visible"));
		field4Value.put("color", field4ValueJSON.getString("color"));
		configObject.setField4Value(field4Value);

		// Load the related data configuration.
		RelatedDataConfiguration rdc1 = new RelatedDataConfiguration();

		// Extract the REl 1 header label.
		Log.d(TAG, jobj.getJSONObject("Relations").toString());
		JSONObject rel1 = jobj.getJSONObject("Relations").getJSONObject("Rel1");

		// Log.d(TAG,rel1.toString());

		rdc1.setRelationID(rel1.getString("RelationID"));

		JSONObject rel1HeaderlabelJSON = rel1.getJSONObject("Header")
				.getJSONObject("label");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel1headerLabel = new HashMap<String, String>();
		rel1headerLabel.put("name", rel1HeaderlabelJSON.getString("name"));
		rel1headerLabel
				.put("visible", rel1HeaderlabelJSON.getString("visible"));
		rel1headerLabel.put("color", rel1HeaderlabelJSON.getString("color"));
		// put it into rdc1.
		rdc1.setHeaderLabel(rel1headerLabel);

		JSONObject rel1HeaderValueJSON = rel1.getJSONObject("Header")
				.getJSONObject("value");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel1headerValue = new HashMap<String, String>();
		rel1headerValue.put("name", rel1HeaderValueJSON.getString("name"));
		rel1headerValue
				.put("visible", rel1HeaderValueJSON.getString("visible"));
		rel1headerValue.put("color", rel1HeaderValueJSON.getString("color"));
		// put it into rdc1.
		rdc1.setHeaderValue(rel1headerValue);

		// Extract the rel1 field1 label and value.
		JSONObject rel1Field1labelJSON = rel1.getJSONObject("Field1")
				.getJSONObject("label");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel1Field1Label = new HashMap<String, String>();
		rel1Field1Label.put("name", rel1Field1labelJSON.getString("name"));
		rel1Field1Label
				.put("visible", rel1Field1labelJSON.getString("visible"));
		rel1Field1Label.put("color", rel1Field1labelJSON.getString("color"));
		// put it into rdc1.
		rdc1.setField1Label(rel1Field1Label);

		JSONObject rel1Field1ValueJSON = rel1.getJSONObject("Field1")
				.getJSONObject("value");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel1Field1Value = new HashMap<String, String>();
		rel1Field1Value.put("name", rel1Field1ValueJSON.getString("name"));
		rel1Field1Value
				.put("visible", rel1Field1ValueJSON.getString("visible"));
		rel1Field1Value.put("color", rel1Field1ValueJSON.getString("color"));
		// put it into rdc1.
		rdc1.setField1Value(rel1Field1Value);

		// Rel Field2.

		JSONObject rel1Field2labelJSON = rel1.getJSONObject("Field2")
				.getJSONObject("label");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel1Field2Label = new HashMap<String, String>();
		rel1Field2Label.put("name", rel1Field2labelJSON.getString("name"));
		rel1Field2Label
				.put("visible", rel1Field2labelJSON.getString("visible"));
		rel1Field2Label.put("color", rel1Field2labelJSON.getString("color"));
		// put it into rdc1.
		rdc1.setField2Label(rel1Field2Label);

		JSONObject rel1Field2ValueJSON = rel1.getJSONObject("Field2")
				.getJSONObject("value");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel1Field2Value = new HashMap<String, String>();
		rel1Field2Value.put("name", rel1Field2ValueJSON.getString("name"));
		rel1Field2Value
				.put("visible", rel1Field2ValueJSON.getString("visible"));
		rel1Field2Value.put("color", rel1Field2ValueJSON.getString("color"));
		// put it into rdc1.
		rdc1.setField2Value(rel1Field2Value);

		// put rdc into ConfigObject.
		configObject.setRel1(rdc1);

		RelatedDataConfiguration rdc2 = new RelatedDataConfiguration();

		// Extract the Rel 1 header label.
		JSONObject rel2 = jobj.getJSONObject("Relations").getJSONObject("Rel2");

		rdc2.setRelationID(rel2.getString("RelationID"));

		JSONObject rel2HeaderlabelJSON = rel2.getJSONObject("Header")
				.getJSONObject("label");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel2headerLabel = new HashMap<String, String>();
		rel2headerLabel.put("name", rel2HeaderlabelJSON.getString("name"));
		rel2headerLabel
				.put("visible", rel2HeaderlabelJSON.getString("visible"));
		rel2headerLabel.put("color", rel2HeaderlabelJSON.getString("color"));
		// put it into rdc2.
		rdc2.setHeaderLabel(rel2headerLabel);

		JSONObject rel2HeaderValueJSON = rel2.getJSONObject("Header")
				.getJSONObject("value");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel2headerValue = new HashMap<String, String>();
		rel2headerValue.put("name", rel2HeaderValueJSON.getString("name"));
		rel2headerValue
				.put("visible", rel2HeaderValueJSON.getString("visible"));
		rel2headerValue.put("color", rel2HeaderValueJSON.getString("color"));
		// put it into rdc2.
		rdc2.setHeaderValue(rel2headerValue);

		// Extract the rel1 field1 label and value.
		JSONObject rel2Field1labelJSON = rel2.getJSONObject("Field1")
				.getJSONObject("label");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel2Field1Label = new HashMap<String, String>();
		rel2Field1Label.put("name", rel2Field1labelJSON.getString("name"));
		rel2Field1Label
				.put("visible", rel2Field1labelJSON.getString("visible"));
		rel2Field1Label.put("color", rel2Field1labelJSON.getString("color"));
		// put it into rdc2.
		rdc2.setField1Label(rel2Field1Label);

		JSONObject rel2Field1ValueJSON = rel2.getJSONObject("Field1")
				.getJSONObject("value");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel2Field1Value = new HashMap<String, String>();
		rel2Field1Value.put("name", rel2Field1ValueJSON.getString("name"));
		rel2Field1Value
				.put("visible", rel2Field1ValueJSON.getString("visible"));
		rel2Field1Value.put("color", rel2Field1ValueJSON.getString("color"));

		// put it into rdc2.
		rdc2.setField1Value(rel2Field1Value);

		// Rel Field2.
		// Extract the rel1 field1 label and value.
		JSONObject rel2Field2labelJSON = rel2.getJSONObject("Field2")
				.getJSONObject("label");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel2Field2Label = new HashMap<String, String>();
		rel2Field2Label.put("name", rel2Field2labelJSON.getString("name"));
		rel2Field2Label
				.put("visible", rel2Field2labelJSON.getString("visible"));
		rel2Field2Label.put("color", rel2Field2labelJSON.getString("color"));
		// put it into rdc1.
		rdc2.setField2Label(rel2Field2Label);

		JSONObject rel2Field2ValueJSON = rel2.getJSONObject("Field2")
				.getJSONObject("value");
		// Create a temp for storing into RelatedDataConfigurations.
		Map<String, String> rel2Field2Value = new HashMap<String, String>();
		rel2Field2Value.put("name", rel2Field2ValueJSON.getString("name"));
		rel2Field2Value
				.put("visible", rel2Field2ValueJSON.getString("visible"));
		rel2Field2Value.put("color", rel2Field2ValueJSON.getString("color"));
		// put it into rdc1.
		rdc2.setField2Value(rel2Field2Value);

		// put rdc into ConfigObject.
		configObject.setRel2(rdc2);

	}

	private void setRelDataField() throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, JSONException {

		// Setting the tabs .
		TabHost th = (TabHost) findViewById(R.id.tabhost);
		th.setup();

		// Rel1 Tab
		ListView listView1 = (ListView) findViewById(R.id.listView1);
		Log.d(TAG, configObject.getRel1().getRelationID());

		// I Have to get the relData configuration Object as a list.
		List<RelatedDataConfiguration> RDC_LIST1 = entityObject.getRelDataList(
				configObject.getRel1().getRelationID(), configObject);

		TabSpec rel1Spec = th.newTabSpec("REL 1");
		rel1Spec.setContent(R.id.tab1);
		if (RDC_LIST1 != null) {
			rel1Spec.setIndicator(configObject.getRel1().getHeaderLabel()
					.get("name")
					+ " " + "(" + RDC_LIST1.size() + ")");

			// Setup the custom adapter.
			final MyListViewData customAdapter = new MyListViewData(this,
					R.layout.listviewitem, RDC_LIST1);
			Log.d(TAG, "GOING INTO THE CUSTOM ADAPTER");
			listView1.setAdapter(customAdapter);
			// Set the listeners.
			listView1.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int pos, long arg3) {

					String ID = customAdapter.getItem(pos).getKEY();
					Intent intent = new Intent(DetailedView.this,
							DetailedView.class);
					intent.putExtra("SCHEMA TYPE", configObject.getRel1()
							.getRelationID());
					intent.putExtra("ID", ID);
					finish();
					startActivity(intent);
					Log.d(TAG, ID);

					Log.d(TAG, "SUCCESSFULL REL ");

				}

			});
		} else {
			Log.d(TAG, configObject.getRel1().getHeaderLabel().get("name"));
			rel1Spec.setIndicator(configObject.getRel1().getHeaderLabel()
					.get("name")
					+ " " + "(0)");
		}
		th.addTab(rel1Spec);

		// Rel2 Tab
		ListView listView2 = (ListView) findViewById(R.id.listView2);

		List<RelatedDataConfiguration> RDC_LIST2 = entityObject.getRelDataList(
				configObject.getRel2().getRelationID(), configObject);

		TabSpec rel2Spec = th.newTabSpec("REL 2");
		rel2Spec.setContent(R.id.tab2);

		if (RDC_LIST2 != null) {
			rel2Spec.setIndicator(configObject.getRel2().getHeaderLabel()
					.get("name")
					+ " " + "(" + RDC_LIST2.size() + ")");
			// Setup the custom adapter.
			final MyListViewData customAdapter = new MyListViewData(this,
					R.layout.listviewitem, RDC_LIST2);

			listView2.setAdapter(customAdapter);

			// Set Rel2 Listener.

			listView2.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int pos, long arg3) {

					String ID = customAdapter.getItem(pos).getKEY();
					Intent intent = new Intent(DetailedView.this,
							DetailedView.class);
					intent.putExtra("SCHEMA TYPE", configObject.getRel2()
							.getRelationID());
					intent.putExtra("ID", ID);
					finish();
					startActivity(intent);
					Log.d(TAG, ID);

					Log.d(TAG, "SUCCESSFULL REL ");

				}

			});
		} else
			rel2Spec.setIndicator(configObject.getRel2().getHeaderLabel()
					.get("name")
					+ " " + "(0)");

		th.addTab(rel2Spec);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detailed_view);

		RelativeLayout layout = (RelativeLayout) findViewById(R.id.relativeLayout);

		Intent intent = getIntent();

		setMapButtonListener(); // Set MapButton Listener.
		EntityFactory factory_Object = new EntityFactory(); // Instantiate a
															// factory object.
		entityObject = factory_Object.getEntity(intent
				.getStringExtra("SCHEMA TYPE")); // Obtain the entity Object via
													// Interface reference.
		setBackGroundColor(intent.getStringExtra("SCHEMA TYPE"), layout);

		dbhelper = new DataBaseHelper(this); // Fetch data from Local DB into
												// the bean object.
		try {
			Log.d(TAG, intent.getStringExtra("SCHEMA TYPE"));

			loadConfig(intent.getStringExtra("SCHEMA TYPE"));

			dbhelper.openDataBase(); // Check if database exists ... to be added
										// later.
			String query = "SELECT * FROM EntityTable where SchemaType='"
					+ intent.getStringExtra("SCHEMA TYPE")
					+ "' and ENTITY_ID='" + intent.getStringExtra("ID") + "';"; // Use
																				// coloumn
																				// name
																				// variables
																				// in
																				// the
																				// query.
			Log.d(TAG, query);

			entityObject = Populate_Entity.populateEntity(entityObject,
					dbhelper.Query(query), this, configObject); // Fetch the
																// partner
																// details along
																// with relData

			setUIFields(); // Set the data.

			setRelDataField(); // Set the Related Data Tabs.

		}// end try
		catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setBackGroundColor(String schemaType, RelativeLayout layout) {

		if (schemaType.equals("Partner"))
			layout.setBackgroundColor(Color.parseColor("#fff0f0"));
		else if (schemaType.equals("Contact"))
			layout.setBackgroundColor(Color.parseColor("#ffff99"));

	}

}
