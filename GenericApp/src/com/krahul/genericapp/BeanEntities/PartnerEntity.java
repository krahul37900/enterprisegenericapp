package com.krahul.genericapp.BeanEntities;

import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.krahul.genericapp.ConfigReader;
import com.krahul.genericapp.Configuration;
import com.krahul.genericapp.RelatedDataConfiguration;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

//metadata and uri not collected.

public class PartnerEntity implements IEntity {

	private String BpRole;
	private String Street;
	private String BusinessPartnerID;
	private String Legalform;
	private String ChangedAt;
	private String AddressValEndDate;
	private String Building;
	private String PhoneNumber;
	private String CompanyName;
	private String Country;
	private String CurrencyCode;
	private String City;
	private String AddressStartValDate;
	private String ChangedBy;
	private String PostalCode;
	private String WebAddress;
	private String FaxNumber;
	private String CreatedBy;
	private String EmailAddress;
	private String AddressType;
	private String CreatedAt;
	private double latitude;
	private double longitude;

	// private List<ContactRelData> contactRelData;
	// private List<SalesOrderRelData> salesOrderRelData;

	private String contactRelJSON;
	private String SalesOrderRelJSON;
	private static final String TAG = "IN PartnerEntity";

	public String getAddress() {
		return Building + " " + Street + " " + City + " " + Country + " "
				+ PostalCode;
	}

	public double getLatitude() {
		return latitude;
	}/*
	 * public List<ContactRelData> getContactRelData() { return contactRelData;
	 * } public void setContactRelData(List<ContactRelData> contactRelData) {
	 * this.contactRelData = contactRelData; } public List<SalesOrderRelData>
	 * getSalesOrderRelData() { return salesOrderRelData; } public void
	 * setSalesOrderRelData(List<SalesOrderRelData> salesOrderRelData) {
	 * this.salesOrderRelData = salesOrderRelData; }
	 */

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getBpRole() {
		return BpRole;
	}

	public void setBpRole(String bpRole) {
		BpRole = bpRole;
	}

	public String getStreet() {
		return Street;
	}

	public void setStreet(String street) {
		Street = street;
	}

	public String getBusinessPartnerID() {
		return BusinessPartnerID;
	}

	public void setBusinessPartnerID(String businessPartnerID) {
		BusinessPartnerID = businessPartnerID;
	}

	public String getLegalform() {
		return Legalform;
	}

	public void setLegalform(String legalform) {
		Legalform = legalform;
	}

	public String getChangedAt() {
		return ChangedAt;
	}

	public void setChangedAt(String changedAt) {
		ChangedAt = changedAt;
	}

	public String getAddressValEndDate() {
		return AddressValEndDate;
	}

	public void setAddressValEndDate(String addressValEndDate) {
		AddressValEndDate = addressValEndDate;
	}

	public String getBuilding() {
		return Building;
	}

	public void setBuilding(String building) {
		Building = building;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getAddressStartValDate() {
		return AddressStartValDate;
	}

	public void setAddressStartValDate(String addressStartValDate) {
		AddressStartValDate = addressStartValDate;
	}

	public String getChangedBy() {
		return ChangedBy;
	}

	public void setChangedBy(String changedBy) {
		ChangedBy = changedBy;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}

	public String getWebAddress() {
		return WebAddress;
	}

	public void setWebAddress(String webAddress) {
		WebAddress = webAddress;
	}

	public String getFaxNumber() {
		return FaxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		FaxNumber = faxNumber;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public String getAddressType() {
		return AddressType;
	}

	public void setAddressType(String addressType) {
		AddressType = addressType;
	}

	public String getCreatedAt() {
		return CreatedAt;
	}

	public void setCreatedAt(String createdAt) {
		CreatedAt = createdAt;
	}

	/*
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.krahul.genericapp.BeanEntities.IEntity#setData(android.database.Cursor
	 * , com.krahul.genericapp.BeanEntities.IEntity)
	 * 
	 * Set the Interface specific bean object for a specific View.
	 */

	@Override
	public IEntity setData(Cursor results, IEntity entityObj, Context context,
			Configuration configObject) throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {

		PartnerEntity temp = (PartnerEntity) entityObj; // Case interface to the
														// EntityType.

		results.moveToFirst();
		try {
			JSONObject resultsJSON = new JSONObject(results.getString(results
					.getColumnIndex("Data")));

			// Set Data.

			setData(configObject.getHeaderValue().get("name"),
					resultsJSON.getString(configObject.getHeaderValue().get(
							"name")), entityObj);
			setData(configObject.getField1Value().get("name"),
					resultsJSON.getString(configObject.getField1Value().get(
							"name")), entityObj);
			setData(configObject.getField2Value().get("name"),
					resultsJSON.getString(configObject.getField2Value().get(
							"name")), entityObj);
			setData(configObject.getField3Value().get("name"),
					resultsJSON.getString(configObject.getField3Value().get(
							"name")), entityObj);
			setData(configObject.getField4Value().get("name"),
					resultsJSON.getString(configObject.getField4Value().get(
							"name")), entityObj);

			// Set RelData.
			JSONObject reldata_json = new JSONObject(results.getString(results
					.getColumnIndex("RelData")));
			JSONArray contacts_relData = reldata_json.getJSONArray("Contact");
			temp.setContactRelJSON(contacts_relData.toString());

			JSONArray salesorder_relData = reldata_json
					.getJSONArray("SalesOrders");
			temp.setSalesOrderRelJSON(salesorder_relData.toString());

		} catch (JSONException e) {

			e.printStackTrace();
		}
		return temp;

	}

	public String getContactRelJSON() {
		return contactRelJSON;
	}

	public void setContactRelJSON(String contactRelJSON) {
		this.contactRelJSON = contactRelJSON;
	}

	public String getSalesOrderRelJSON() {
		return SalesOrderRelJSON;
	}

	public void setSalesOrderRelJSON(String salesOrderRelJSON) {
		SalesOrderRelJSON = salesOrderRelJSON;
	}

	/*
	 * private List<SalesOrderRelData> setSalesOrderRelation(JSONObject
	 * reldata_json) throws JSONException {
	 * 
	 * List<SalesOrderRelData> sord =null;
	 * 
	 * 
	 * if(reldata_json.isNull("SalesOrders") == false){
	 * 
	 * JSONArray sos = reldata_json.getJSONArray("SalesOrders"); sord = new
	 * ArrayList<SalesOrderRelData>(); for( int i =0 ; i < sos.length() ; i++){
	 * JSONObject single_order = sos.getJSONObject(i); String SOID =
	 * single_order.getString("_id"); Double GrossAmount =
	 * single_order.getDouble("GrossAmount"); String Currency =
	 * single_order.getString("CurrencyCode");
	 * 
	 * SalesOrderRelData sord_single = new SalesOrderRelData();
	 * sord_single.setSOID(SOID); sord_single.setGross_Amount(GrossAmount);
	 * sord_single.setCurrency_Code(Currency);
	 * 
	 * sord.add(sord_single);
	 * 
	 * }//end for .
	 * 
	 * }//end of if . return sord; }
	 * 
	 * private List<ContactRelData> setContactRelation(JSONObject reldata_json)
	 * throws JSONException {
	 * 
	 * List<ContactRelData> crd=null;
	 * 
	 * if ( reldata_json.isNull("Contact") == false ){
	 * 
	 * JSONArray cts = reldata_json.getJSONArray("Contact"); crd = new
	 * ArrayList<ContactRelData>(); for(int i=0 ; i < cts.length() ; i++){
	 * JSONObject single_contact = cts.getJSONObject(i); String ID
	 * =single_contact.getString("_id"); String fname=
	 * single_contact.getString("FirstName"); String lname
	 * =single_contact.getString("LastName");
	 * 
	 * ContactRelData crd_single = new ContactRelData();
	 * crd_single.setContactID(ID); crd_single.setContactFirstName(fname);
	 * crd_single.setContactLastName(lname);
	 * 
	 * crd.add(crd_single);
	 * 
	 * }//end of for .
	 * 
	 * 
	 * }//end of if return crd;
	 * 
	 * }
	 */

	public void setData(String funcName, String Value, IEntity entityObj)
			throws NoSuchMethodException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {

		String methodName = "set" + funcName;
		// Object [] paramList = new Object[] {Value};
		Class[] paramString = new Class[1];
		paramString[0] = String.class;
		java.lang.reflect.Method method = entityObj.getClass().getMethod(
				methodName, paramString);

		// Set the params

		method.invoke(entityObj, new String(Value));

	}

	@Override
	public String getData(String funcName) throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {

		String methodName = "get" + funcName;
		java.lang.reflect.Method method = this.getClass().getMethod(methodName);
		return (String) method.invoke(this, (Object[]) null);

	}

	

	@Override
	public List<RelatedDataConfiguration> getRelDataList(String relationID,Configuration configObject)
			throws JSONException {
		
		List<RelatedDataConfiguration> data = null;

		if (relationID.equals("Contact")) {
			if (getContactRelJSON() != null) {
				data = new ArrayList<RelatedDataConfiguration>();
				
				JSONArray contactJSON = new JSONArray(getContactRelJSON());

				for (int i = 0; i < contactJSON.length(); i++) {

					JSONObject temp = contactJSON.getJSONObject(i);

					
					//Temp Objects for filling up list of RelDataConfig Object.
					RelatedDataConfiguration RDC_temp = new RelatedDataConfiguration();
					Map<String,String> Field1Value = new HashMap<String,String>();
					Map<String,String> Field2Value = new HashMap<String,String>();
					String GuID;
					
					//Extract from Contact JSON	 from the Config File	
					String id = temp.getString("_id");
					String fname = temp.getString(configObject.getRel1().getField1Value().get("name"));
					String lname = temp.getString(configObject.getRel1().getField2Value().get("name"));

					GuID = id;
					Field1Value.put("FIELD1",fname );
					Field2Value.put("FIELD2", lname);
					
					RDC_temp.setKEY(GuID);
					RDC_temp.setField1Value(Field1Value);
					RDC_temp.setField2Value(Field2Value);
									
					
					data.add(RDC_temp);
				}
			}
		} else if (relationID.equals("SalesOrders")) {

			Log.d(TAG,"In SALES ORDERS SETREL DATA");
			
			
			if (getSalesOrderRelJSON() != null) {
				data = new ArrayList<RelatedDataConfiguration>();
				JSONArray salesJSON = new JSONArray(getSalesOrderRelJSON());

				for (int i = 0; i < salesJSON.length(); i++) {

					JSONObject temp = salesJSON.getJSONObject(i);


					//Temp Objects for filling up list of RelDataConfig Object.
					RelatedDataConfiguration RDC_temp = new RelatedDataConfiguration();
					Map<String,String> Field2Value = new HashMap<String,String>();
					Map<String,String> Field1Value = new HashMap<String,String>();
					String GuID;
					
					String SOID = temp.getString("_id");
					String GrossAmount =  temp.getString(configObject.getRel2().getField1Value().get("name"));
					String CurrencyCode = temp.getString(configObject.getRel2().getField2Value().get("name"));
 					
					GuID=SOID;
					Field1Value.put("FIELD1",GrossAmount);
					Field2Value.put("FIELD2",CurrencyCode);
					
					RDC_temp.setKEY(GuID);
					RDC_temp.setField1Value(Field1Value);
					RDC_temp.setField2Value(Field2Value);
					data.add(RDC_temp);
				
				}
			}

		}

		return data;
		
		
	}

	

}
