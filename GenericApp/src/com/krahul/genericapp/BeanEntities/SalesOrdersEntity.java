package com.krahul.genericapp.BeanEntities;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.krahul.genericapp.Configuration;
import com.krahul.genericapp.RelatedDataConfiguration;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

//metadata and business partner uri not collected.

public class SalesOrdersEntity implements IEntity {

	private static final String TAG="IN SALES ORDER ENTITY";
	
	private String SoId;
	private String ChangedAt;
	private String NetAmount;
	private String Note;
	private String BillingStatus;
	private String GrossAmount;
	private String TaxAmountExt;
	private String TaxAmount;
	private String CurrencyCode;
	private String ChangedBy;
	private String CreatedByBp;
	private String ChangedByBp;

	public String getContactRelJSON() {
		return contactRelJSON;
	}

	public void setContactRelJSON(String contactRelJSON) {
		this.contactRelJSON = contactRelJSON;
	}

	public String getPartnerRelJSON() {
		return PartnerRelJSON;
	}

	public void setPartnerRelJSON(String partnerRelJSON) {
		PartnerRelJSON = partnerRelJSON;
	}

	public String getNetAmount() {
		return NetAmount;
	}

	public String getGrossAmount() {
		return GrossAmount;
	}

	public String getTaxAmountExt() {
		return TaxAmountExt;
	}

	public String getTaxAmount() {
		return TaxAmount;
	}

	public String getGrossAmountExt() {
		return GrossAmountExt;
	}

	public String getNetAmountExt() {
		return NetAmountExt;
	}

	private String DeliveryStatus;
	private String CreatedBy;
	private String BuyerId;
	private String GrossAmountExt;
	private String NetAmountExt;
	private String LifeCycleStatus;
	private String contactRelJSON;
	private String PartnerRelJSON;

	public String getCreatedByBp() {
		return CreatedByBp;
	}

	public void setCreatedByBp(String createdByBp) {
		CreatedByBp = createdByBp;
	}

	public String getChangedByBp() {
		return ChangedByBp;
	}

	public void setChangedByBp(String changedByBp) {
		ChangedByBp = changedByBp;
	}

	public void setNetAmount(String netAmount) {
		NetAmount = netAmount;
	}

	public void setGrossAmount(String grossAmount) {
		GrossAmount = grossAmount;
	}

	public void setTaxAmountExt(String taxAmountExt) {
		TaxAmountExt = taxAmountExt;
	}

	public void setTaxAmount(String taxAmount) {
		TaxAmount = taxAmount;
	}

	public void setGrossAmountExt(String grossAmountExt) {
		GrossAmountExt = grossAmountExt;
	}

	public void setNetAmountExt(String netAmountExt) {
		NetAmountExt = netAmountExt;
	}

	private String CreatedAt;
	private String BuyerName;

	public String getSoId() {
		return SoId;
	}

	public void setSoId(String soId) {
		SoId = soId;
	}

	public String getChangedAt() {
		return ChangedAt;
	}

	public void setChangedAt(String changedAt) {
		ChangedAt = changedAt;
	}

	public String getNote() {
		return Note;
	}

	public void setNote(String note) {
		Note = note;
	}

	public String getBillingStatus() {
		return BillingStatus;
	}

	public void setBillingStatus(String billingStatus) {
		BillingStatus = billingStatus;
	}

	public String getCurrencyCode() {
		return CurrencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}

	public String getChangedBy() {
		return ChangedBy;
	}

	public void setChangedBy(String changedBy) {
		ChangedBy = changedBy;
	}

	public String getDeliveryStatus() {
		return DeliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		DeliveryStatus = deliveryStatus;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getBuyerId() {
		return BuyerId;
	}

	public void setBuyerId(String buyerId) {
		BuyerId = buyerId;
	}

	/*
	 * public String getLineItemsURI() { return LineItemsURI; } public void
	 * setLineItemsURI(String lineItemsURI) { LineItemsURI = lineItemsURI; }
	 */

	public String getLifeCycleStatus() {
		return LifeCycleStatus;
	}

	public void setLifeCycleStatus(String lifeCycleStatus) {
		LifeCycleStatus = lifeCycleStatus;
	}

	public String getCreatedAt() {
		return CreatedAt;
	}

	public void setCreatedAt(String createdAt) {
		CreatedAt = createdAt;
	}

	public String getBuyerName() {
		return BuyerName;
	}

	public void setBuyerName(String buyerName) {
		BuyerName = buyerName;
	}

	public void setData(String funcName, String Value, IEntity entityObj)
			throws NoSuchMethodException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {

		String methodName = "set" + funcName;
		// Object [] paramList = new Object[] {Value};
		Class[] paramString = new Class[1];
		paramString[0] = String.class;
		java.lang.reflect.Method method = entityObj.getClass().getMethod(
				methodName, paramString);

		// Set the params

		method.invoke(entityObj, new String(Value));

	}

	@Override
	public IEntity setData(Cursor results, IEntity entityObj, Context context,
			Configuration configObject) throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {

		SalesOrdersEntity temp = (SalesOrdersEntity) entityObj; // Case interface to the
													// EntityType.
		results.moveToFirst();

		try {
			JSONObject resultsJSON = new JSONObject(results.getString(results
					.getColumnIndex("Data")));

			// Set data
			setData(configObject.getHeaderValue().get("name"),
					resultsJSON.getString(configObject.getHeaderValue().get(
							"name")), entityObj);
			setData(configObject.getField1Value().get("name"),
					resultsJSON.getString(configObject.getField1Value().get(
							"name")), entityObj);
			setData(configObject.getField2Value().get("name"),
					resultsJSON.getString(configObject.getField2Value().get(
							"name")), entityObj);
			setData(configObject.getField3Value().get("name"),
					resultsJSON.getString(configObject.getField3Value().get(
							"name")), entityObj);
			setData(configObject.getField4Value().get("name"),
					resultsJSON.getString(configObject.getField4Value().get(
							"name")), entityObj);

			// Set RelData.
			JSONObject reldata_json = new JSONObject(results.getString(results
					.getColumnIndex("RelData")));

			JSONArray partner_relData = reldata_json.getJSONArray("Partner");
			temp.setPartnerRelJSON(partner_relData.toString());

			JSONArray contacts_relData = reldata_json.getJSONArray("Contact");
			temp.setContactRelJSON(contacts_relData.toString());

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return temp;
	}

	@Override
	public String getData(String funcName) throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		String methodName = "get" + funcName;
		java.lang.reflect.Method method = this.getClass().getMethod(methodName);
		return (String) method.invoke(this, (Object[]) null);
	}
/*
	@Override
	public List<String> getRelData(String funcName) throws JSONException {
		List<String> listViewValues = null;

		if (funcName.equals("Contact")) {

			if (getContactRelJSON() != null) {

				listViewValues = new ArrayList<String>();

				JSONArray contactJSON = new JSONArray(getContactRelJSON());

				for (int i = 0; i < contactJSON.length(); i++) {

					JSONObject temp = contactJSON.getJSONObject(i);

					String id = temp.getString("_id");
					// String fname = temp.getString("FirstName");
					// String lname = temp.getString("LastName");

					listViewValues.add(id);
				}
			} // end of if .
		} else if (funcName.equals("Partner")) {

			if (getPartnerRelJSON() != null) {
				listViewValues = new ArrayList<String>();
				JSONArray partnerJSON = new JSONArray(getPartnerRelJSON());

				for (int i = 0; i < partnerJSON.length(); i++) {

					JSONObject temp = partnerJSON.getJSONObject(i);

					String SOID = temp.getString("_id");
					listViewValues.add(SOID);
				}
			}// end if .
		}

		return listViewValues;

	}
*/

	@Override
	public List<RelatedDataConfiguration> getRelDataList(String relationID,Configuration configObject)
			throws JSONException {
		List<RelatedDataConfiguration> data = null;

		if (relationID.equals("Contact")) {
			if (getContactRelJSON() != null) {
				data = new ArrayList<RelatedDataConfiguration>();
				
				JSONArray contactJSON = new JSONArray(getContactRelJSON());

				for (int i = 0; i < contactJSON.length(); i++) {

					JSONObject temp = contactJSON.getJSONObject(i);

					
					//Temp Objects for filling up list of RelDataConfig Object.
					RelatedDataConfiguration RDC_temp = new RelatedDataConfiguration();
					Map<String,String> Field1Value = new HashMap<String,String>();
					Map<String,String> Field2Value = new HashMap<String,String>();
					String GuID;
					
					//Extract from Contact JSON	 from the Config File	
					String id = temp.getString("_id");
					String fname = temp.getString(configObject.getRel1().getField1Value().get("name"));
					String lname = temp.getString(configObject.getRel1().getField2Value().get("name"));

					GuID = id;
					Field1Value.put("FIELD1",fname );
					Field2Value.put("FIELD2", lname);
					
					RDC_temp.setKEY(GuID);
					RDC_temp.setField1Value(Field1Value);
					RDC_temp.setField2Value(Field2Value);
									
					
					data.add(RDC_temp);
				}
			}
		} else if (relationID.equals("Partner")) {

			Log.d(TAG,"In SALES ORDERS SETREL DATA");
			
			
			if (getPartnerRelJSON() != null) {
				data = new ArrayList<RelatedDataConfiguration>();
				JSONArray partnerJSON = new JSONArray(getPartnerRelJSON());

				for (int i = 0; i < partnerJSON.length(); i++) {

					JSONObject temp = partnerJSON.getJSONObject(i);


					//Temp Objects for filling up list of RelDataConfig Object.
					RelatedDataConfiguration RDC_temp = new RelatedDataConfiguration();
					Map<String,String> Field1Value = new HashMap<String,String>();
					String GuID;
					
					String partnerID = temp.getString("_id");
					String CompanyName =  temp.getString(configObject.getRel2().getField1Value().get("name"));
				
 					
					GuID=partnerID;
					Field1Value.put("FIELD1",CompanyName);
					
					RDC_temp.setKEY(GuID);
					RDC_temp.setField1Value(Field1Value);
					data.add(RDC_temp);
				
				}
			}

		}

		return data;
		
	}
	

	
}
