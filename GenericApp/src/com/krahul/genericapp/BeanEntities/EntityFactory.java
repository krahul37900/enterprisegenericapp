package com.krahul.genericapp.BeanEntities;

public class EntityFactory {
	/*
	 * Return the appropriate Entity Bean Object based on the schema type.
	 */
	public IEntity getEntity(String SCHEMA_TYPE) {

		if (SCHEMA_TYPE.equals("Partner")) {
			return new PartnerEntity();
		} else if (SCHEMA_TYPE.equals("Contact")) {
			return new ContactEntity();
		} else if (SCHEMA_TYPE.equals("SalesOrders")) {
			return new SalesOrdersEntity();
		} else
			return null;

	}
}
