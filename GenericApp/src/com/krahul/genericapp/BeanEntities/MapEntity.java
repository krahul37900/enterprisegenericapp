package com.krahul.genericapp.BeanEntities;

import com.google.android.gms.maps.model.Marker;

public class MapEntity {

	private Marker mapMarker;
	private String SchemaType;
	private String ID;
	private Double Latitude;
	private Double Longitude;
	private float Color;

	public float getColor() {
		return Color;
	}

	public Marker getMapMarker() {
		return mapMarker;
	}

	public void setMapMarker(Marker mapMarker) {
		this.mapMarker = mapMarker;
	}

	public void setColor(float color) {
		Color = color;
	}

	public String getSchemaType() {
		return SchemaType;
	}

	public void setSchemaType(String schemaType) {
		SchemaType = schemaType;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public Double getLatitude() {
		return Latitude;
	}

	public void setLatitude(Double latitude) {
		Latitude = latitude;
	}

	public Double getLongitude() {
		return Longitude;
	}

	public void setLongitude(Double longitude) {
		Longitude = longitude;
	}

}
