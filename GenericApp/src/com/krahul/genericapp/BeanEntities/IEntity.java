package com.krahul.genericapp.BeanEntities;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.json.JSONException;

import com.krahul.genericapp.Configuration;
import com.krahul.genericapp.RelatedDataConfiguration;

import android.content.Context;
import android.database.Cursor;

public interface IEntity {

	IEntity setData(Cursor result, IEntity entityObj, Context context,
			Configuration configObject) throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, JSONException;

	String getData(String funcName) throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException;

	

	List<RelatedDataConfiguration> getRelDataList(String relationID,Configuration configObject)throws JSONException;

	

}
