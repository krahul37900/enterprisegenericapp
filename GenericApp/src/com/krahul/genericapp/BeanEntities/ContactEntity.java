package com.krahul.genericapp.BeanEntities;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.krahul.genericapp.ConfigReader;
import com.krahul.genericapp.Configuration;
import com.krahul.genericapp.RelatedDataConfiguration;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class ContactEntity implements IEntity {

	private static final String TAG="IN CONTACT ENTITY";
	
	private String Street;
	private String BusinessPartnerId;
	private String AddressValEndDate;
	private String Building;
	private String PhoneNumber;
	private String Country;
	private String LastName;
	private String City;
	private String Title;
	private String AddressValStartDate;
	private String PostalCode;
	private String MiddleName;
	private String NickName;
	private String FaxNumber;
	private String Initials;
	private String EmailAddress;
	private String FirstName;
	private Double Latitude;
	private Double Longitude;
	private String AddressType;
	private String Language;
	private String Sex;
	// private List<PartnerRelData> partnerRelData;
	// private List<SalesOrderRelData> salesOrderRelData;

	private String PartnerRelJSON;
	private String SalesOrderRelJSON;

	public String getAddress() {
		return Building + " " + Street + " " + City + " " + Country;
	}

	/*
	 * public List<PartnerRelData> getPartnerRelData() { return partnerRelData;
	 * } public void setPartnerRelData(List<PartnerRelData> contactRelData) {
	 * this.partnerRelData = contactRelData; } public List<SalesOrderRelData>
	 * getSalesOrderRelData() { return salesOrderRelData; } public void
	 * setSalesOrderRelData(List<SalesOrderRelData> salesOrderRelData) {
	 * this.salesOrderRelData = salesOrderRelData; }
	 */
	public Double getLatitude() {
		return Latitude;
	}

	public String getPartnerRelJSON() {
		return PartnerRelJSON;
	}

	public void setPartnerRelJSON(String contactRelJSON) {
		this.PartnerRelJSON = contactRelJSON;
	}

	public String getSalesOrderRelJSON() {
		return SalesOrderRelJSON;
	}

	public void setSalesOrderRelJSON(String salesOrderRelJSON) {
		SalesOrderRelJSON = salesOrderRelJSON;
	}

	public void setLatitude(Double latitude) {
		Latitude = latitude;
	}

	public Double getLongitude() {
		return Longitude;
	}

	public void setLongitude(Double longitude) {
		Longitude = longitude;
	}

	public String getStreet() {
		return Street;
	}

	public void setStreet(String street) {
		Street = street;
	}

	public String getBusinessPartnerId() {
		return BusinessPartnerId;
	}

	public void setBusinessPartnerId(String businessPartnerId) {
		BusinessPartnerId = businessPartnerId;
	}

	public String getAddressValEndDate() {
		return AddressValEndDate;
	}

	public void setAddressValEndDate(String addressValEndDate) {
		AddressValEndDate = addressValEndDate;
	}

	public String getBuilding() {
		return Building;
	}

	public void setBuilding(String building) {
		Building = building;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getAddressValStartDate() {
		return AddressValStartDate;
	}

	public void setAddressValStartDate(String addressValStartDate) {
		AddressValStartDate = addressValStartDate;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}

	public String getMiddleName() {
		return MiddleName;
	}

	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}

	public String getNickName() {
		return NickName;
	}

	public void setNickName(String nickName) {
		NickName = nickName;
	}

	public String getFaxNumber() {
		return FaxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		FaxNumber = faxNumber;
	}

	public String getInitials() {
		return Initials;
	}

	public void setInitials(String initials) {
		Initials = initials;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getAddressType() {
		return AddressType;
	}

	public void setAddressType(String addressType) {
		AddressType = addressType;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getSex() {
		return Sex;
	}

	public void setSex(String sex) {
		Sex = sex;
	}

	public void setData(String funcName, String Value, IEntity entityObj)
			throws NoSuchMethodException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {

		String methodName = "set" + funcName;
		// Object [] paramList = new Object[] {Value};
		Class[] paramString = new Class[1];
		paramString[0] = String.class;
		java.lang.reflect.Method method = entityObj.getClass().getMethod(
				methodName, paramString);

		// Set the params

		method.invoke(entityObj, new String(Value));

	}

	@Override
	public IEntity setData(Cursor results, IEntity entityObj, Context context,
			Configuration configObject) throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, JSONException {

		ContactEntity temp = (ContactEntity) entityObj; // Case interface to the
												// EntityType.

		results.moveToFirst();

		try {

			JSONObject resultsJSON = new JSONObject(results.getString(results
					.getColumnIndex("Data")));

			// Set Data
			setData(configObject.getHeaderValue().get("name"),
					resultsJSON.getString(configObject.getHeaderValue().get(
							"name")), entityObj);
			setData(configObject.getField1Value().get("name"),
					resultsJSON.getString(configObject.getField1Value().get(
							"name")), entityObj);
			setData(configObject.getField2Value().get("name"),
					resultsJSON.getString(configObject.getField2Value().get(
							"name")), entityObj);
			setData(configObject.getField3Value().get("name"),
					resultsJSON.getString(configObject.getField3Value().get(
							"name")), entityObj);
			setData(configObject.getField4Value().get("name"),
					resultsJSON.getString(configObject.getField4Value().get(
							"name")), entityObj);
			// Set RelData.
			JSONObject reldata_json = new JSONObject(results.getString(results
					.getColumnIndex("RelData")));

			JSONArray partner_relData = reldata_json.getJSONArray("Partner");
			temp.setPartnerRelJSON(partner_relData.toString());

			JSONArray salesorder_relData = reldata_json
					.getJSONArray("SalesOrders");
			temp.setSalesOrderRelJSON(salesorder_relData.toString());

		} catch (JSONException e) {

			e.printStackTrace();
		}
		return temp;

	}

	@Override
	public String getData(String funcName) throws NoSuchMethodException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		String methodName = "get" + funcName;
		java.lang.reflect.Method method = this.getClass().getMethod(methodName);
		return (String) method.invoke(this, (Object[]) null);

	}

	@Override
	public List<RelatedDataConfiguration> getRelDataList(String relationID,Configuration configObject)
			throws JSONException {
		List<RelatedDataConfiguration> data = null;

		if (relationID.equals("Partner")) {
			if (getPartnerRelJSON()!= null) {
				data = new ArrayList<RelatedDataConfiguration>();
				
				JSONArray partnerJSON = new JSONArray(getPartnerRelJSON());

				for (int i = 0; i < partnerJSON.length(); i++) {

					JSONObject temp = partnerJSON.getJSONObject(i);

					
					//Temp Objects for filling up list of RelDataConfig Object.
					RelatedDataConfiguration RDC_temp = new RelatedDataConfiguration();
					Map<String,String> Field1Value = new HashMap<String,String>();
					String GuID;
					
					//Extract from Contact JSON	 from the Config File	
					String id = temp.getString("_id");
					String CompanyName = temp.getString(configObject.getRel1().getField1Value().get("name"));
					
					GuID = id;
					Field1Value.put("FIELD1",CompanyName );
					
					RDC_temp.setKEY(GuID);
					RDC_temp.setField1Value(Field1Value);
									
					
					data.add(RDC_temp);
				}
			}
		} else if (relationID.equals("SalesOrders")) {

			Log.d(TAG,"In SALES ORDERS SETREL DATA");
			
			
			if (getSalesOrderRelJSON() != null) {
				data = new ArrayList<RelatedDataConfiguration>();
				JSONArray salesJSON = new JSONArray(getSalesOrderRelJSON());

				for (int i = 0; i < salesJSON.length(); i++) {

					JSONObject temp = salesJSON.getJSONObject(i);


					//Temp Objects for filling up list of RelDataConfig Object.
					RelatedDataConfiguration RDC_temp = new RelatedDataConfiguration();
					Map<String,String> Field2Value = new HashMap<String,String>();
					Map<String,String> Field1Value = new HashMap<String,String>();
					String GuID;
					
					String SOID = temp.getString("_id");
					String GrossAmount =  temp.getString(configObject.getRel2().getField1Value().get("name"));
					String CurrencyCode = temp.getString(configObject.getRel2().getField2Value().get("name"));
 					
					GuID=SOID;
					Field1Value.put("FIELD1",GrossAmount);
					Field2Value.put("FIELD2",CurrencyCode);
					
					RDC_temp.setKEY(GuID);
					RDC_temp.setField1Value(Field1Value);
					RDC_temp.setField2Value(Field2Value);
					data.add(RDC_temp);
				
				}
			}

		}
		return data;
	}

	

	

	

}
